﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ebaySearcher.com.ebay.developer.finding.api;
using System.Windows.Input;
using MVVMbase;

namespace ebaySearcher
{
    [Serializable]
    public class EbayMonitorOptionModel
    {
        private ICommand StartMonitorCommand { get; set; }
        private ICommand StopMonitorCommand { get; set; }
        public EbayMonitorOptionModel()
        {

//            StartMonitorCommand = new RelayCommand(StartMonitorCommandExecute);
//            StopMonitorCommand = new RelayCommand(StopMonitorCommandExecute);
        }

        private void StopMonitorCommandExecute()
        {
        }

        private void StartMonitorCommandExecute()
        {
        }
        public String MonitorName { get; set; }

        public AspectsTemplateModel AspectsTemplate { get; set; }
        public List<SearchItemModel> SearchItems { get; set; }
        public string KeyText { get; set; }


        private event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged != null)
            {
                foreach (string propertyName in propertyNames)
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

                //PropertyChanged(this, new PropertyChangedEventArgs("HasError"));
            }
        }
    }

    [Serializable]
    public class SearchItemModel
    {

        public string Country { get; set; }
        public string GalleryURL { get; set; }
        public int ItemId { get; set; }
        public ListingInfoModel ListingInfo { get; set; }
        public string Location { get; set; }
        public List<string> PaymentMethod { get; set; }
        public int PostalCode { get; set; }
        public string ViewItemURL { get; set; }
        public string ItemTitle { get; set; }
        public double ItemPrice { get; set; }
        public string TimeLeft { get; set; }
        public double ShippingPrice { get; set; }

        public double AmountPrice
        {
            get { return ItemPrice + ShippingPrice; }
        }

    }

    [Serializable]
    public class ListingInfoModel
    {
        public string EndTime { get; set; }
        public string StartTime { get; set; }
        public string ListingType { get; set; }
    }
}
