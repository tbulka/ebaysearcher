﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ebaySearcher.com.ebay.developer.finding.api;


namespace ebaySearcher
{
    public class CustomFindingService : FindingService
    {
        public string OperationName { get; set; }

        public CustomFindingService(string operationName)
        {
            OperationName = operationName;
        }

        protected override WebRequest GetWebRequest(Uri uri)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)base.GetWebRequest(uri);
                request.Headers.Add("X-EBAY-SOA-SECURITY-APPNAME", ConfigurationManager.AppSettings["appID"]);
                request.Headers.Add("X-EBAY-SOA-OPERATION-NAME", OperationName);
                //request.Headers.Add("X-EBAY-SOA-SERVICE-NAME", "FindingService");
                request.Headers.Add("X-EBAY-SOA-MESSAGE-PROTOCOL", "SOAP12");
                request.Headers.Add("X-EBAY-SOA-SERVICE-VERSION", "1.13.0");
                //request.Headers.Add("X-EBAY-SOA-GLOBAL-ID", "EBAY-US");
                return request;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
