﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ebaySearcher.com.ebay.developer;
using Microsoft.TeamFoundation.MVVM;


namespace ebaySearcher
{
    public partial class MainWindow : Window
    {
        private EbaySearcherModelView modelView;
        public MainWindow()
        {
            InitializeComponent();
            modelView = new EbaySearcherModelView();
            DataContext = modelView;
        }

        private void MainWindow_OnClosing(object sender, CancelEventArgs e)
        {
            if (modelView.MonitorCollection.Count > 0)
            {
                string directory = "SearchTemplate";
                string fileName = string.Format(@"{0}/SearchTempl", directory);
                SaveAsBinaryFormat(new List<EbayMonitorOptionModel>(modelView.MonitorCollection), fileName, directory);
            }
        }

        private void SaveAsBinaryFormat(object objGraph, string fileName, string directory)
        {
            var binFormat = new BinaryFormatter();

            if (!Directory.Exists(@directory))
            {
                Directory.CreateDirectory(directory);
            }

            using (Stream fStream = new FileStream(fileName,
                FileMode.Create, FileAccess.Write, FileShare.None))
            {
                binFormat.Serialize(fStream, objGraph);
            }
        }


    }
}
