﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using ebaySearcher.com.ebay.developer;

//using eBay.

namespace ebaySearcher
{
    public class EbayTradingApiConnect
    {
        public string OperationName { get; set; }

        private readonly string signinURL;
        private readonly string callName;
        private readonly string siteId;
        private readonly string appId;
        private readonly string devId;
        private readonly string certId;
        private readonly string version;
        private readonly string eBayToken;
        private readonly string endpoint;
        public EbayTradingApiConnect(string operationName)
        {
            OperationName = operationName;
            callName = OperationName;
            signinURL = ConfigurationManager.AppSettings["ApiServerUrl"];
            siteId = ConfigurationManager.AppSettings["eBayUserSiteId"];
            appId = ConfigurationManager.AppSettings["AppId"];
            devId = ConfigurationManager.AppSettings["DevId"];
            certId = ConfigurationManager.AppSettings["CertId"];
            version = ConfigurationManager.AppSettings["Version"];
            eBayToken = ConfigurationManager.AppSettings["ApiToken"];
            endpoint = string.Format("{0}?callname={1}&siteid={2}&appid={3}&version={4}&routing=default", signinURL, callName, siteId, appId, version);
        }

        public eBayAPIInterfaceService GetEbayAccess()
        {
            eBayAPIInterfaceService service = new eBayAPIInterfaceService();
            service.Url = endpoint;
            service.RequesterCredentials = new CustomSecurityHeaderType();
            service.RequesterCredentials.eBayAuthToken = eBayToken;    // use your token
            service.RequesterCredentials.Credentials = new UserIdPasswordType();
            service.RequesterCredentials.Credentials.AppId = appId;
            service.RequesterCredentials.Credentials.DevId = devId;
            service.RequesterCredentials.Credentials.AuthCert = certId;
            return service;
        }

        //public CustomSecurityHeaderType SetEbayToken()
        //{
        //    CustomSecurityHeaderType cred = new CustomSecurityHeaderType();
        //    cred.eBayAuthToken = eBayToken;
        //    return cred;
        //}
    }
}
