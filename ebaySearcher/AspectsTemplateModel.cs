﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ebaySearcher.CategoriesAndAspects;

namespace ebaySearcher
{
    [Serializable]
    public class AspectsTemplateModel
    {
        public string Uniquekey { get; set; }

        public string TemplateName { get; set; }
        public List<AspectsModel> Aspects { get; set; }

        public List<AspectsModel> SelectedAspects
        {
            get { return Aspects.Where(x => x.CheckedAspectValues.Count > 0).ToList(); }
        }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        public AspectsTemplateModel(string templateName, List<AspectsModel> aspects, int categoryId, string categoryName, string uniqueKey)
        {
            TemplateName = templateName;
            Aspects = aspects;
            CategoryId = categoryId;
            CategoryName = categoryName;
            Uniquekey = uniqueKey;
        }

        public override string ToString()
        {
            return TemplateName;
        }
    }
}
