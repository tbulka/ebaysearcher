﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Xml.Serialization;
using ebaySearcher.com.ebay.developer.finding.api;
using ebaySearcher.CategoriesAndAspects;
using ebaySearcher.Converter;
using ebaySearcher.EditAspectTemplate;
using Microsoft.TeamFoundation.MVVM;
using MVVMbase;
using RelayCommand = MVVMbase.RelayCommand;

namespace ebaySearcher
{
    public sealed class EbaySearcherModelView : NotificationObject
    {
        private AspectsTemplateModel _selectedTemplateName;
        public EbayMonitorOptionModel selectedSearchTemplate;

        public EbaySearcherModelView()
        {
            //            if (EbayApiData.Instance().CategoryTypes == null || EbayApiData.Instance().CategoryTypes.Count == 0)
            //            {
            //                Task.Run(() => { EbayApiData.Instance().SetEbayApiCategories(); }).ContinueWith(i =>
            //                {
            //                    EbayConnection = true;
            //                    RaisePropertyChanged("EbayConnection");
            //                });
            //            }
            MonitorCollection = new ObservableCollection<EbayMonitorOptionModel>();
            //            MonitorCollection.Add(new EbayMonitorOptionModel() { MonitorName = "AddSearchCode123" });
            //            RaisePropertyChanged("MonitorCollection");
            AddSearchCommand = new RelayCommand(AddSearchCommandExecute);
            CategoryNameAndId = "Select category aspect template";
            RaisePropertyChanged("CategoryNameAndId");
            EditAspectCommand = new RelayCommand<AspectsModel>(EditAspectCommandExecute);
            SaveCommand = new RelayCommand(SaveCommandExecute);
            EditTemplateCommand = new RelayCommand(EditTemplateCommandExecute);
            SearchResult = new BatchedObservableCollection<SearchItem>();
            TESTCommand = new RelayCommand(TESTCommandExecute);
            DeleteSearchTemplCommand = new RelayCommand(DeleteSearchTemplCommandExecute);
            ShowAspectsCommand = new RelayCommand(ShowAspectsCommandExecute);
            CountriesStore = new List<string>();
            AspectsTemplates = new ObservableCollection<AspectsTemplateModel>();
            if (Directory.Exists(@"SaveTemplates"))
            {
                foreach (var file in
                    new DirectoryInfo(@"SaveTemplates").EnumerateFiles("*.dat", SearchOption.AllDirectories))
                {
                    var template = (AspectsTemplateModel)LoadFromBinaryFile<AspectsTemplateModel>(file.FullName);
                    AspectsTemplates.Add(new AspectsTemplateModel(file.Name.Replace(file.Extension, ""),
                        template.Aspects, template.CategoryId, template.TemplateName, template.Uniquekey));
                }
                RaisePropertyChanged("AspectsTemplates");
            }
            else Directory.CreateDirectory("SaveTemplates");

            if (Directory.Exists(@"SearchTemplate"))
            {
                string path = @"SearchTemplate/SearchTempl";
                if (File.Exists(path))
                {
                    var template = (IEnumerable<EbayMonitorOptionModel>)LoadFromBinaryFile<IEnumerable<EbayMonitorOptionModel>>(path);
                    MonitorCollection = new ObservableCollection<EbayMonitorOptionModel>(template);
                }
                RaisePropertyChanged("MonitorCollection");
            }
            else Directory.CreateDirectory("SaveTemplates");
        }

        private void DeleteSearchTemplCommandExecute()
        {
            if (SelectedSearchTemplate != null)
            {
                MessageBoxResult result = MessageBox.Show("Are you sure?", "!", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                {
                    MonitorCollection.Remove(SelectedSearchTemplate);
                    RaisePropertyChanged("MonitorCollection");
                }
            }
        }

        private object LoadFromBinaryFile<T>(string fileName)
        {
            var binFormat = new BinaryFormatter();
            T deserialize;
            using (Stream fStream = File.OpenRead(fileName))
            {
                deserialize = (T)binFormat.Deserialize(fStream);
            }
            return deserialize;
        }

        public ObservableCollection<EbayMonitorOptionModel> MonitorCollection { get; set; }
        public ObservableCollection<SearchItem> SearchResult { get; set; }

        public ObservableCollection<AspectsModel> Aspects
        {
            get
            {
                if (AllAspects.IsNullOrEmpty())
                {
                    return null;
                }
                return new ObservableCollection<AspectsModel>(AllAspects.Where(x => x.AspectsValue
                    .Any(z => z.IsChecked || (z.PriceCondition != null &&
                                              (z.PriceCondition.From != default(int) ||
                                               z.PriceCondition.To != default(int))))).ToList());
            }
            set { AllAspects = value; }
        }

        public EbayMonitorOptionModel SelectedSearchTemplate
        {
            get { return selectedSearchTemplate; }
            set
            {
                selectedSearchTemplate = value;
                if (value != null)
                {
                    SelectedTemplateName = value.AspectsTemplate;
                }
            }
        }

        public ObservableCollection<AspectsModel> AllAspects { set; get; }
        public string TemplateSaveAs { get; set; }
        public ObservableCollection<AspectsTemplateModel> AspectsTemplates { get; set; }
        public AspectsTemplateModel SelectedAspectTemplate { get; set; }
        public string SelectedSite { get; set; }
        public string TemplateName { get; set; }
        public List<string> CountriesStore { get; set; }
        public string CategoryNameAndId { get; set; }
        public ICommand SaveCommand { get; set; }
        public ICommand DeleteSearchTemplCommand { get; set; }
        
        public ICommand AddSearchCommand { get; set; }
        public ICommand EditTemplateCommand { get; set; }
        public RelayCommand<AspectsModel> EditAspectCommand { get; set; }
        public bool EbayConnection { get; set; }
        public ICommand TESTCommand { get; set; }
        public ICommand ShowAspectsCommand { get; set; }
        public string SearchText { get; set; }
        public string SearchNotification { get; set; }

        public AspectsTemplateModel SelectedTemplateName
        {
            get { return _selectedTemplateName; }
            set
            {
                if (value != null)
                {
                    if (AspectsTemplates.Any(x => x.Uniquekey == value.Uniquekey))
                    {
                        Aspects =
                            new ObservableCollection<AspectsModel>(
                                AspectsTemplates.Single(x => x.TemplateName == value.TemplateName).Aspects);
                        CategoryNameAndId =
                            string.Format("{0} (id:{1})",
                                AspectsTemplates.Single(x => x.TemplateName == value.TemplateName).CategoryName,
                                AspectsTemplates.Single(x => x.TemplateName == value.TemplateName).CategoryId);
                        SelectedAspectTemplate = AspectsTemplates.Single(x => x.TemplateName == value.TemplateName);
                        TemplateSaveAs = value.TemplateName;
                        RaisePropertyChanged("TemplateSaveAs");
                        RaisePropertyChanged("Aspects");
                        RaisePropertyChanged("CategoryNameAndId");
                        _selectedTemplateName = AspectsTemplates.Single(x => x.Uniquekey == value.Uniquekey);
                        RaisePropertyChanged("SelectedTemplateName");
                    }
                    else
                    {
                        _selectedTemplateName = null;
                        Aspects =
                            new ObservableCollection<AspectsModel>(value.Aspects);
                        CategoryNameAndId =
                            string.Format("{0} (id: {1})", value.CategoryName, value.CategoryId);
                        SelectedAspectTemplate = value;
                        TemplateSaveAs = value.TemplateName;
                        RaisePropertyChanged("TemplateSaveAs");
                        RaisePropertyChanged("Aspects");
                        RaisePropertyChanged("CategoryNameAndId");
                        RaisePropertyChanged("SelectedTemplateName");
                    }
                }
            }
        }

        private void AddSearchCommandExecute()
        {
            MonitorCollection.Add(new EbayMonitorOptionModel
            {
                AspectsTemplate = SelectedAspectTemplate,
                MonitorName = SelectedAspectTemplate.TemplateName
            });

            RaisePropertyChanged("MonitorCollection");
        }

        private void EditAspectCommandExecute(AspectsModel aspects)
        {
            var editAspectMv = new EditAspectModelView();
            var editAspect = new EditAspect(editAspectMv);
            editAspectMv.AspectValues = new ObservableCollection<AspectItem>(aspects.AspectsValue.ToList());
            editAspect.ShowDialog();
            if (editAspect.DialogResult == true)
            {
                var list = editAspectMv.AspectValues.Except(aspects.AspectsValue).ToList();
                if (list != null && list.Count != 0)
                {
                    aspects.AspectsValue.AddRange(list);
                }
                RaisePropertyChanged("Aspects");
            }
        }

        private void SaveCommandExecute()
        {
            var templateSaveAsName = TemplateSaveAs;
            if (!string.IsNullOrEmpty(TemplateSaveAs))
            {
                var template = AspectsTemplates.Single(x => x.TemplateName == SelectedTemplateName.TemplateName);
                SaveAsBinaryFormat(new AspectsTemplateModel(templateSaveAsName,
                    template.Aspects, template.CategoryId, template.CategoryName, Guid.NewGuid().ToString()),
                    string.Format(@"SaveTemplates\{0}.dat", templateSaveAsName));

                if (Directory.Exists(@"SaveTemplates"))
                {
                    if (AspectsTemplates.Any(x => x.TemplateName == templateSaveAsName))
                    {
                        AspectsTemplates.Remove(AspectsTemplates.Single(x => x.TemplateName == templateSaveAsName));
                    }

                    foreach (var file in
                        new DirectoryInfo(@"SaveTemplates").EnumerateFiles(string.Format(@"{0}.dat", templateSaveAsName),
                            SearchOption.AllDirectories))
                    {
                        var loadTemplate = (AspectsTemplateModel)LoadFromBinaryFile<AspectsTemplateModel>(file.FullName);
                        AspectsTemplates.Add(new AspectsTemplateModel(file.Name.Replace(file.Extension, ""),
                            loadTemplate.Aspects, loadTemplate.CategoryId, loadTemplate.TemplateName, loadTemplate.Uniquekey));
                    }
                    RaisePropertyChanged("AspectsTemplates");
                }
            }
        }

        private void EditTemplateCommandExecute()
        {
            if (SelectedTemplateName != null)
            {
                var aspectsModelView = new AspectsModelView();
                aspectsModelView.SelectMode = false;
                aspectsModelView.VisibilityDelete = true;
                aspectsModelView.TemplateName = SelectedTemplateName.TemplateName;
                aspectsModelView.Aspects = AllAspects;
                var aspects = new Aspects(aspectsModelView);
                aspects.ShowDialog();
                if (aspects.DialogResult == true)
                {
                    if (Directory.Exists(@"SaveTemplates"))
                    {
                        if (aspectsModelView.DeleteTemplate)
                        {
                            if (AspectsTemplates.Any(x => x.TemplateName == aspectsModelView.TemplateName))
                            {
                                AspectsTemplates.Remove(
                                    AspectsTemplates.Single(x => x.TemplateName == aspectsModelView.TemplateName));
                            }
                        }
                        else
                        {
                            if (AspectsTemplates.Any(x => x.TemplateName == aspectsModelView.TemplateName))
                            {
                                string uniquekey = Guid.NewGuid().ToString();
                                AspectsTemplateModel aspect =
                                    AspectsTemplates.Single(x => x.TemplateName == aspectsModelView.TemplateName);
                                aspect.Uniquekey = uniquekey;

                                SaveAsBinaryFormat(aspect,
                                    string.Format(@"SaveTemplates\{0}.dat", aspectsModelView.TemplateName));
                            }
                        }
                        RaisePropertyChanged("AspectsTemplates");
                    }
                    else
                    {
                        Directory.CreateDirectory("SaveTemplates");
                    }
                    RaisePropertyChanged("Aspects");
                }
            }
        }

        private void ShowAspectsCommandExecute()
        {
            var aspectsModelView = new AspectsModelView();
            aspectsModelView.SelectMode = true;
            aspectsModelView.VisibilityDelete = false;
            var aspects = new Aspects(aspectsModelView);
            aspects.ShowDialog();
            if (aspects.DialogResult == true)
            {
                var aspectsList = aspectsModelView.Aspects;

                if (!string.IsNullOrEmpty(aspectsModelView.TemplateName))
                {
                    string uniquekey = Guid.NewGuid().ToString();
                    SaveAsBinaryFormat(new AspectsTemplateModel(aspectsModelView.TemplateName,
                        aspectsList.ToList(), aspectsModelView.SelectedCategoryId, aspectsModelView.SelectedCategoryName, uniquekey),
                        string.Format(@"SaveTemplates\{0}.dat", aspectsModelView.TemplateName));
                    AspectsTemplates.Add(new AspectsTemplateModel(aspectsModelView.TemplateName
                        , aspectsList.ToList(), aspectsModelView.SelectedCategoryId,
                        aspectsModelView.SelectedCategoryName, uniquekey));
                }
                RaisePropertyChanged("AspectsTemplates");
            }
        }

        private void SaveAsXmlFormat(object objGraph, string fileName)
        {
            if (!Directory.Exists(@"SaveTemplates"))
            {
                Directory.CreateDirectory("SaveTemplates");
            }

            using (Stream fStream = new FileStream(fileName,
                FileMode.Create, FileAccess.Write, FileShare.None))
            {
                var xmlFormat = new XmlSerializer(typeof(List<AspectsModel>));
                xmlFormat.Serialize(fStream, objGraph);
            }
        }

        private void SaveAsBinaryFormat(object objGraph, string fileName)
        {
            var binFormat = new BinaryFormatter();

            if (!Directory.Exists(@"SaveTemplates"))
            {
                Directory.CreateDirectory("SaveTemplates");
            }

            using (Stream fStream = new FileStream(fileName,
                FileMode.Create, FileAccess.Write, FileShare.None))
            {
                binFormat.Serialize(fStream, objGraph);
            }
        }

        private AspectsTemplateModel LoadFromBinaryFile(string fileName)
        {
            var binFormat = new BinaryFormatter();
            AspectsTemplateModel deserializeApects;
            using (Stream fStream = File.OpenRead(fileName))
            {
                deserializeApects = (AspectsTemplateModel)binFormat.Deserialize(fStream);
            }
            return deserializeApects;
        }

        private void TESTCommandExecute()
        {
            var service = new CustomFindingService("findItemsAdvanced");

            var template = AspectsTemplates.Single(x => x.TemplateName == SelectedTemplateName.TemplateName);
            var request = new FindItemsAdvancedRequest();
            request.categoryId = new[] { template.CategoryId.ToString() };

            var filterList = new List<AspectFilter>();
            foreach (var aspects in Aspects
                .Where(x => x.Title != "Condition" & x.Title != "Price" & x.Title != "Show only"))
            {
                var filter = new AspectFilter();
                filter.aspectName = aspects.Title;
                filter.aspectValueName = aspects.CheckedAspectValues.Select(x => x.Title).ToArray();
                filterList.Add(filter);
            }
            request.aspectFilter = filterList.ToArray();
            request.sortOrderSpecified = true;
            request.outputSelector = new[] { OutputSelectorType.ConditionHistogram, OutputSelectorType.AspectHistogram };
            request.sortOrder = SortOrderType.StartTimeNewest;
            var itemFilter = new List<ItemFilter>();

            //itemFilter.Add(new ItemFilter { name = ItemFilterType.Currency, value = new string[] { "USD" } });

            foreach (var aspects in Aspects
                .Where(x => x.Title == "Condition" || x.Title == "Price" || x.Title == "Show only"))
            {
                switch (aspects.Title)
                {
                    case "Condition":
                        var conditionFilter = new ItemFilter();
                        conditionFilter.name = ItemFilterType.Condition;
                        conditionFilter.value = aspects.CheckedAspectValues.Select(x => x.Title).ToArray();
                        itemFilter.Add(conditionFilter);
                        break;
                    case "Price":
                        //price from
                        var priceFromFilter = new ItemFilter();
                        priceFromFilter.name = ItemFilterType.MinPrice;
                        priceFromFilter.value = new[] { aspects.CheckedAspectValues.Single().PriceCondition.From.ToString() };
                        priceFromFilter.paramName = "Currency";
                        priceFromFilter.paramValue = "USD";
                        itemFilter.Add(priceFromFilter);

                        //price to
                        var priceToFilter = new ItemFilter();
                        priceToFilter.name = ItemFilterType.MaxPrice;
                        priceToFilter.value = new[] { aspects.CheckedAspectValues.Single().PriceCondition.To.ToString() };
                        priceToFilter.paramName = "Currency";
                        priceToFilter.paramValue = "USD";
                        itemFilter.Add(priceToFilter);

                        break;
                    case "Show only":
                        var showOnlyFilterr = new ItemFilter();
                        showOnlyFilterr.name = ItemFilterType.BestOfferOnly;
                        showOnlyFilterr.value = new[] { "true" };
                        itemFilter.Add(showOnlyFilterr);
                        break;
                }
            }
            request.itemFilter = itemFilter.ToArray();
            request.keywords = "iphone";
            var response = service.findItemsAdvanced(request);

            foreach (var item in response.searchResult.item)
            {
                MonitorCollection.Add(new EbayMonitorOptionModel());
            }
            //List<ItemFilter> item = new List<ItemFilter>()
            //{
            //    new ItemFilter {name = ItemFilterType.BestOfferOnly, value = new string[] {"true"}},
            //    new ItemFilter {name = ItemFilterType.Condition, value = new string[] {"true"}}

            //};

            // var aspectsList = new List<AspectsModel>();
            //CustomFindingService service = new CustomFindingService("getHistograms");
            //service.Url = "http://svcs.ebay.com/services/search/FindingService/v1";
            ////service.v
            //GetHistogramsRequest abcd = new GetHistogramsRequest();
            //abcd.categoryId = categoryId.ToString();
            //GetHistogramsResponse response = service.getHistograms(abcd);
        }

        private void TESTCommandExecute1()
        {
            //List<CategoryType> selected2 =
            //    allCategories.Where(x => x.CategoryName == "Cell Phones & Smartphones").ToList();

            var service = new CustomFindingService("getHistograms");
            service.Url = "http://svcs.ebay.com/services/search/FindingService/v1";
            var abcd = new GetHistogramsRequest();
            abcd.categoryId = "9355";
            var response = service.getHistograms(abcd);
            //service = new CustomFindingService("getSearchKeywordsRecommendation");
            //service.Url = "http://svcs.ebay.com/services/search/FindingService/v1";
            ////request
            //GetSearchKeywordsRecommendationRequest abcd2 = new GetSearchKeywordsRecommendationRequest();
            //abcd2.keywords = "iphone";
            //GetSearchKeywordsRecommendationResponse response2 = service.getSearchKeywordsRecommendation(abcd2);

            //getca
            //.aspectHistogramContainer.aspect.ToList();
            //var rr = response.aspectHistogramContainer.aspect;
            //if (response != null)
            //{
            /*AspectsModelView aspectsModelView = new AspectsModelView();
             Aspects.Aspects aspects = new Aspects.Aspects(aspectsModelView);
             if (response.conditionHistogramContainer.conditionHistogram != null)
             {
                 TextWrapping wrap = (response.conditionHistogramContainer.conditionHistogram.Length > 20) ? TextWrapping.Wrap : TextWrapping.NoWrap;
                 List<AspectItem> aspectsItemList = response.conditionHistogramContainer.conditionHistogram
                     .Select(x => new AspectItem(x.condition.conditionDisplayName, x.count, wrap, DataTemplateSelectorEnum.CheckBox)).ToList();

                 aspectsModelView.Aspects.Add(new AspectsModel("Condition", aspectsItemList));
             }

             if (response.aspectHistogramContainer != null)
             {
                 foreach (Aspect aspect in response.aspectHistogramContainer.aspect)
                 {
                     TextWrapping wrap = (aspect.valueHistogram.Length > 20) ? TextWrapping.Wrap : TextWrapping.NoWrap;

             //        aspectsModelView.Aspects.Add(new AspectsModel(aspect.name, aspect.valueHistogram
             //            .Select(x => new AspectItem(x.valueName, x.count, wrap, DataTemplateSelectorEnum.CheckBox)).ToList()));
             //    }
             //}

             //var price = new List<AspectItem>();
             //price.Add(new AspectItem(String.Empty, 0, TextWrapping.NoWrap, DataTemplateSelectorEnum.Price));
             aspectsModelView.Aspects.Add(new AspectsModel("Price", price));

             //.Select(x => new AspectItem(x.valueName, x.count, wrap, DataTemplateSelectorEnum.CheckBox)).ToList()));

             //aspectsModelView.Aspects = new ObservableCollection<AspectsModel>(response.aspectHistogramContainer.aspect.ToList());
             ////fore
             //aspectsModelView.Conditions = new ObservableCollection<ConditionHistogram>(response.conditionHistogramContainer.conditionHistogram.ToList());
             aspects.Show();*/
            //}
        }

        private void Search()
        {
            /*
			try
			{
                //LstCategories.Items.Clear();
	
				GetCategoriesCall apicall = new GetCategoriesCall(Context);

				// Enable HTTP compression to reduce the download size.
				apicall.EnableCompression = true;

				apicall.DetailLevelList.Add(DetailLevelCodeType.ReturnAll);
                apicall.ViewAllNodes = true;
				
                //if (TxtLevel.Text.Length > 0)
					apicall.LevelLimit = 1;

                //if (TxtParent.Text.Length > 0)
                //{
                //    apicall.CategoryParent = new StringCollection();
                //    apicall.CategoryParent.Add(TxtParent.Text);

                //}
				CategoryTypeCollection cats = apicall.GetCategories();
		
				foreach (CategoryType category in cats)
				{
                    Categories.Add(category);
                    //string[] listparams = new string[8];
                    //listparams[0] = category.CategoryID;
                    //listparams[1] = category.CategoryLevel.ToString();
                    //listparams[2] = category.CategoryName;
                    //listparams[3] = category.CategoryParentID[0].ToString();
                    //listparams[4] = category.LeafCategory.ToString();
                    //listparams[5] = category.BestOfferEnabled.ToString();
                    //listparams[6] = apicall.ApiResponse.MinimumReservePrice.ToString();

                    //ListViewItem vi = new ListViewItem(listparams);
                    //this.LstCategories.Items.Add(vi);

				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}*/


            //StringBuilder strResult = new StringBuilder();


            /*///////////////////
             * try
            {
                //Category category = new Category();
                //category.categoryName
                CustomFindingService service = new CustomFindingService();
                service.Url = "http://svcs.ebay.com/services/search/FindingService/v1";
                FindItemsByKeywordsRequest request = new FindItemsByKeywordsRequest();
                FindItemsByCategoryRequest requestCAT = new FindItemsByCategoryRequest();

                // Setting the required proterty value
                request.keywords = SearchText.Trim();
                //ItemFilter filter = new ItemFilter();
                //filter.
                // Setting the pagination 
                //PaginationInput pagination = new PaginationInput();
                //pagination.entriesPerPageSpecified = true;
                //pagination.entriesPerPage = 25;
                //pagination.pageNumberSpecified = true;
                //pagination.pageNumber = 1;
                //request.paginationInput = pagination;

                // Sorting the result
                //request.

                ItemFilter if1 = new ItemFilter();
                ItemFilter if2 = new ItemFilter();
                if1.name = ItemFilterType.ListingType;
                if1.value = new string[] { "Model:iPhone 4s" };

                request.sortOrderSpecified = true;
                request.sortOrder = SortOrderType.CurrentPriceHighest;


                FindItemsByKeywordsResponse response = service.findItemsByKeywords(request);
                if (response.searchResult.count > 0)
                    foreach (SearchItem searchItem in response.searchResult.item)
                        SearchResult.Add(searchItem);
            }
            catch (Exception ex)
            {
                SearchNotification = string.Format("Error: " + ex.Message);
            }
             ///////////////////////////////////////////////////////////////////////////////////////////////
             */


            /*
             IPaginationInput pagination = new PaginationInput();

                    pagination.entriesPerPageSpecified = true;
                    pagination.entriesPerPage = 100;
                    pagination.pageNumberSpecified = true;
                    pagination.pageNumber = curPage;
                    request.paginationInput = pagination;

                    ItemFilter if1 = new ItemFilter();
                    ItemFilter if2 = new ItemFilter();
                    if1.name = ItemFilterType.ListingType;
                    if1.value = new string[] { "Auction" };




                    ItemFilter[] ifa = new ItemFilter[1];
                    ifa[0] = if1;
                    request.itemFilter = ifa;

                    FindItemsByKeywordsResponse response = client.findItemsByKeywords(request);


                    foreach (var item in response.searchResult.item)
                    {
                         // EDIT
                         if (item.listingInfo.startTime.CompareTo(DateTime.UtcNow) > -1) // -1 is earlyer; 0 is same; +1 is later then
                         {
                           if (item.listingInfo.startTime.CompareTo(DateTime.UtcNow.AddDays(-2)) == -1 )
                           {
                               // You have an Item that was started between now and two days ago.
                               // Do something

                           }
                        }
                        // END EDIT

                        tw.WriteLine(item.viewItemURL.ToString());
                        links.Add(item.viewItemURL.ToString());
                    }
             */
        }
    }
}


//            EbayTradingApiConnect connect = new EbayTradingApiConnect("GetCategorySpecifics");

//            eBayAPIInterfaceService service = connect.GetEbayAccess();
//            //GetCategoriesRequestType gercatreq = new GetCategoriesRequestType();
//            //gercatreq.CategorySiteID = "0";
//            //gercatreq.Version = "935";

//            //gercatreq.DetailLevel = new DetailLevelCodeType[] { DetailLevelCodeType.ReturnAll };
//            //GetCategoriesResponseType getcatpes = service.GetCategories(gercatreq);


//            /* GetCategoryFeaturesRequestType featuresReq = new GetCategoryFeaturesRequestType();

//             featuresReq.CategoryID = "9355";
//             featuresReq.AllFeaturesForCategorySpecified = true;
//             featuresReq.Version = "935";
//             GetCategoryFeaturesResponseType featuresResponse = service.GetCategoryFeatures(featuresReq);
//             */

//            GetCategorySpecificsRequestType req = new GetCategorySpecificsRequestType();
//            req.Version = "935";
//            req.IncludeConfidenceSpecified = true;
//            req.IncludeConfidence = true;
//            //req.Name = "Model";
//            req.MaxValuesPerName = 100;
//            req.MaxNames = 30;
//            //req.MessageID = "iphone";
//            //req.OutputSelector = new string[] { "iphone", "nokia" };
//            req.CategoryID = new string[] { "9355" };
//            //req.CategorySpecific = new CategoryItemSpecificsType[1];
//            //req.CategorySpecific[0] = new CategoryItemSpecificsType(); //CategoryID = "9355";
//            //req.CategorySpecific[0].ItemSpecifics = new NameValueListType[1];
//            //req.CategorySpecific[0].CategoryID = "9355";
//            //req.CategorySpecific[0].ItemSpecifics[0] = new NameValueListType();
//            //req.CategorySpecific[0].ItemSpecifics[0].Name = "Model";
//            //req.CategorySpecific[0].ItemSpecifics[0].Source = ItemSpecificSourceCodeType.Product;
//            //req.CategorySpecific[0].ItemSpecifics[0].SourceSpecified = true;
//            //req.CategorySpecific[0].ItemSpecifics[0].Value = new string[] { "iphone" };


//            GetCategorySpecificsResponseType resp = service.GetCategorySpecifics(req);
//            //GetCategoryFeaturesRequestType request = new GetCategoryFeaturesRequestType();
//            //request.CategoryID = "9355";
//            //request.Version = "935";
//            //GetCategoryFeaturesResponseType resp = service.GetCategoryFeatures(request);
//            //GetCategorySpecifics(request);
//            ////GeteBayOfficialTimeResponseType response = service.GeteBayOfficialTime(request);
//            ////Console.WriteLine("The time at eBay headquarters in San Jose, California, USA, is:");
//            ////Console.WriteLine(response.Timestamp);
//            ////// Make the call to GeteBayOfficialTime
//            //GeteBayOfficialTimeRequestType request = new GeteBayOfficialTimeRequestType();
//            //request.Version = "405";
//            //GeteBayOfficialTimeResponseType response = service.GeteBayOfficialTime(request);
//public ApiContext SetApiContext()
//{
//    ApiContext cxt = new ApiContext();

//    // set api server address
//    cxt.SoapApiServerUrl = ConfigurationManager.AppSettings[ConfigKeyApiUrl];
//    // set token
//    ApiCredential ac = new ApiCredential();
//    string token = ConfigurationManager.AppSettings[ConfigKeyApiToken];
//    ac.eBayToken = token;
//    cxt.ApiCredential = ac;

//    // initialize log.
//    ApiLogManager logManager = null;
//    string logPath = ConfigurationManager.AppSettings[ConfigKeyLogFile];
//    if (logPath.Length > 0)
//    {
//        logManager = new ApiLogManager();

//        logManager.EnableLogging = true;

//        logManager.ApiLoggerList = new ApiLoggerCollection();
//        ApiLogger log = new FileLogger(logPath, true, true, true);
//        logManager.ApiLoggerList.Add(log);
//    }
//    cxt.ApiLogManager = logManager;

//    return cxt;
//}


//apiContext = SetApiContext();
//SearchResult = new ObservableCollection<SearchItem>();
//Categories = new ObservableCollection<CategoryType>();
//allCategories = new List<CategoryType>();
//apiContexts = new List<ApiContext>();
//try
//{
//    //getcate
//    //CategoriesDownloader downloader = new CategoriesDownloader(apiContext);//"20081"  Cell Phones & Smartphones
//    //allCategories = downloader.GetAllCategories().Cast<CategoryType>().ToList();
//    //Categories = new ObservableCollection<CategoryType>(allCategories.Where(x => x.CategoryLevel == 1).ToList());
//}
//catch (ApiException ex)
//{
//    MessageBox.Show(ex.Message);
//}
//catch (Exception)
//{
//}
//foreach (string counrty in Enum.GetNames(typeof(SiteCodeType)))
//    CountriesStore.Add(counrty);
////Adva
//StoreCustomCategoryType cat = new StoreCustomCategoryType();
//cat.CategoryID = 162894;
//cat.ChildCategory.
//GetCategoriesCall rootCategoriesCall = new GetCategoriesCall(apiContext);
//rootCategoriesCall.Site = SiteCodeType.US;
//rootCategoriesCall.DetailLevelList.Add(DetailLevelCodeType.ReturnAll);
//rootCategoriesCall.LevelLimit = 1;
//var categories = rootCategoriesCall.GetCategories().Cast<CategoryType>();

/*    GetHistogramsRequest abcd = new GetHistogramsRequest();
abcd.setCategoryId(CatID.toString());
GetHistogramsResponse bb = port.getHistograms(abcd);
advanceResponse.setAspectHistogramContainer(bb.getAspectHistogramContainer());
AspectHistogramContainer aspectContainer=new AspectHistogramContainer();
aspectContainer=advanceResponse.getAspectHistogramContainer();
aspectContainer.setDomainDisplayName(keywords);
aspectContainer.setDomainName(keywords);            
List<Aspect> aspectList=aspectContainer.getAspect();
for(Aspect aspect:aspectList)
{               
System.out.println(aspect.getName());
List<AspectValueHistogram> aspectValueList= aspect.getValueHistogram();
for(AspectValueHistogram aspectValue:aspectValueList)
{
System.out.println(aspectValue.getValueName());
}
}
//AspectFilter filter = new AspectFilter();
////var filt = filter.aspectValueName.ToList();

//Aspect aspect = new Aspect();
////var aspectValueHistogram = aspect.valueHistogram.ToList();

//AspectValueHistogram cValueHistogram = new AspectValueHistogram();
////long d = cValueHistogram.count;

//AspectHistogramContainer container = new AspectHistogramContainer();
////var list = container.aspect.ToList();

CategoryType selected = allCategories.SingleOrDefault(x => x.CategoryID == "20081");
List<CategoryType> selected2 = allCategories.Where(x => x.CategoryName == "Post-1950").ToList();
if (selected != null)
{
    var showSelected = allCategories.Where(x => x.CategoryParentID[0] == selected.CategoryID).ToList();

}
//selected.
//UploadCategories();
//Search();
*/