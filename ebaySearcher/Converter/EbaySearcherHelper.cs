﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace ebaySearcher.Converter
{
    public class EbaySearcherHelper
    {
        public static T LoadFromBinaryFile<T>(string fileName, string directory)
        {
            var binFormat = new BinaryFormatter();
            T deserialize;
            using (Stream fStream = File.OpenRead(string.Format(@"{0}/{1}", directory, fileName)))
            {
                deserialize = (T)binFormat.Deserialize(fStream);
            }
            return deserialize;
        }

        public static void SaveAsBinaryFormat(object objGraph, string fileName, string directory)
        {
            var binFormat = new BinaryFormatter();

            if (!Directory.Exists(@directory))
            {
                Directory.CreateDirectory(directory);
            }

            using (Stream fStream = new FileStream(string.Format(@"{0}/{1}", directory, fileName),
                FileMode.Create, FileAccess.Write, FileShare.None))
            {
                binFormat.Serialize(fStream, objGraph);
            }
        }
    }

    public static class EbaySearcherExtensionHelper
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable == null)
                return true;

            return !enumerable.Any();
        }
    }
}
