﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using ebaySearcher.CategoriesAndAspects;

namespace ebaySearcher
{
    public class SearcherTemplateSelector : DataTemplateSelector
    {
        public DataTemplate AddSearchDataTemplate { get; set; }
        public DataTemplate SimpleSearchDataTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            EbayMonitorOptionModel searchItem = (EbayMonitorOptionModel)item;

            if (searchItem != null)
                switch (searchItem.MonitorName)
                {
                    case "AddSearchCode123":
                        return AddSearchDataTemplate;
                    default:
                        return SimpleSearchDataTemplate; 
                            
                }
            return null;
        }
    }
}
