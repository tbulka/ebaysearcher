﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ebaySearcher.EditAspectTemplate;

namespace ebaySearcher.CategoriesAndAspects
{
    public enum DataTemplateSelectorEnum
    {
        RadioButton,
        CheckBox,
        Price
    }

    [Serializable]
    public class AspectsModel
    {
        public string Title { get; set; }

        private List<AspectItem> aspectValues;

        public List<AspectItem> AspectsValue
        {
            get { return aspectValues; }
            set
            {
                //NotifyPropertyChanged("AspectsValue");
                aspectValues = value;
            }
        }
        public bool AddFuncion { get; set; }
        public List<AspectItem> CheckedAspectValues
        {
            get
            {
                if (Title == "Price" &&
                    (AspectsValue.Single().PriceCondition.From != default(int) || AspectsValue.Single().PriceCondition.To != default(int)))
                {
                    AspectsValue.Single().IsChecked = true;
                    AspectsValue.Single().Title = AspectsValue.Single().PriceCondition.ToString();
                }
                var checkedValuesList = AspectsValue.Where(x => x.IsChecked).ToList();
                return checkedValuesList;
            }
        }
        public AspectsModel()
        {
            AspectsValue = new List<AspectItem>();
        }
        public AspectsModel(string title, List<AspectItem> aspectsValue, bool addFuncion = true)
        {
            Title = title;
            AddFuncion = addFuncion;
            AspectsValue = aspectsValue;
        }

        /*private event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged != null)
            {
                foreach (string propertyName in propertyNames) 
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

                //PropertyChanged(this, new PropertyChangedEventArgs("HasError"));
            }
        }*/

    }

    /* public virtual event PropertyChangedEventHandler PropertyChanged;
     protected virtual void NotifyPropertyChanged(params string[] propertyNames)
     {
         if (PropertyChanged != null)
         {
             foreach (string propertyName in propertyNames) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
             PropertyChanged(this, new PropertyChangedEventArgs("HasError"));
         }
     }*/

    [Serializable]
    public class AspectItem
    {
        public DataTemplateSelectorEnum DataTemplateSelector { get; set; }
        public string Title { get; set; }

        public long Count { get; set; }
        public Price PriceCondition { get; set; }
        public bool IsChecked { get; set; }

        public TextWrapping Wrap { get; set; }

        public AspectItem(string title, long count, TextWrapping wrap, DataTemplateSelectorEnum selectortype)
        {
            Title = title;
            Count = count;
            Wrap = wrap;
            DataTemplateSelector = selectortype;
            if (selectortype.Equals(DataTemplateSelectorEnum.Price))
            {
                PriceCondition = new Price();
            }
        }

        public AspectItem(string title, DataTemplateSelectorEnum selectortype)
        {
            DataTemplateSelector = selectortype;
            Title = title;
        }

        [Serializable]
        public class Price
        {
            public int From { get; set; }
            public int To { get; set; }

            public override string ToString()
            {
                return String.Format("From {0}$ to {1}$", From, To);
            }
        }
    }
}



//Wrap = (aspectsValue.Count() > 20) ? Wrap = TextWrapping.Wrap : Wrap = TextWrapping.NoWrap;
//if (aspectsValue.Count() > 20)
//    Wrap = TextWrapping.Wrap;
//else
//    Wrap = TextWrapping.NoWrap;
//    AspectsValue = aspectsValue.Where(x =>
//    {
//        if (x.Title.Length > 21)
//        {
//            x.Title = x.Title.Insert(21, "\n");
//        }

//        return true;
//    }).ToList();
//else