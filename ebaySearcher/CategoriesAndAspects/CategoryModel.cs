﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ebaySearcher.CategoriesAndAspects
{
    [Serializable]
    public class CategoryModel
    {
        private List<CategoryModel> getCategoriesNextLevel;
        private bool isSelected;

        public CategoryModel(string catName, int catId, int catParentId, int catLevel)
        {
            CategoryName = catName;
            CategoryId = catId;
            CategoryParentId = catParentId;
            CategoryLevel = catLevel;
        }

        public static CategoryModel SelectedCategory { get; set; }
        public string CategoryName { get; set; }
        public int CategoryId { get; set; }
        public int CategoryParentId { get; set; }
        public int CategoryLevel { get; set; }

        public List<CategoryModel> CategoriesNextLevel
        {
            get { return getCategoriesNextLevel; }
            set { getCategoriesNextLevel = value; }
        }

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                if (value)
                {
                    SelectedCategory = this;
                }
                isSelected = value;
            }
        }

        public List<CategoryModel> GetCategoriesNextLevel
        {
            get
            {
                if (getCategoriesNextLevel == null)
                {
                    getCategoriesNextLevel = new List<CategoryModel>();
                    getCategoriesNextLevel.AddRange(
                        EbayApiData.Instance().CategoryTypes.Where(
                            x =>
                                x.CategoryLevel == CategoryLevel + 1 &&
                                int.Parse(x.CategoryParentID[0]) == CategoryId).Select(x =>
                                    new CategoryModel(x.CategoryName, int.Parse(x.CategoryID),
                                        int.Parse(x.CategoryParentID[0]),
                                        x.CategoryLevel)));

                    if (getCategoriesNextLevel.Count != 0)
                    {
                        CategoryName += ">";
                    }
                }
                return getCategoriesNextLevel;
            }
            set { getCategoriesNextLevel = value; }
        }
    }
}