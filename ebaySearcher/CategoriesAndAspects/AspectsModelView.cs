﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using ebaySearcher.com.ebay.developer;
using ebaySearcher.com.ebay.developer.finding.api;
using ebaySearcher.Converter;
using MVVMbase;
//using eBay.Service.Core.Soap;

namespace ebaySearcher.CategoriesAndAspects
{
    public class AspectsModelView : NotificationObject
    {
        private readonly List<AspectItem> showOnly;
        private List<CategoryType> categorytype;
        private List<Task> threadsCount;
        private bool visibility;

        public AspectsModelView()
        {
            UpdateCommand = new RelayCommand(UpdateCommandExecute);
            DeleteCommand = new RelayCommand<Window>(DeleteCommandExecute);
            SaveCommand = new RelayCommand<Window>(SaveCommandExecute);

            if (Directory.Exists(@"Categories"))
            {
                var file = "CategoriesTempl";
                var directory = "Categories";
                if (File.Exists(string.Format("{0}/{1}", directory, file)))
                {
                    var template = EbaySearcherHelper.LoadFromBinaryFile<IEnumerable<CategoryType>>(file, directory).ToList();
                    if (!template.IsNullOrEmpty())
                    {
                        var list = new List<CategoryModel>();
                        EbayApiData.Instance().CategoryTypes = template;
                        list.AddRange(template.Where(x => x.CategoryLevel == 1)
                            .Select(x => new CategoryModel(x.CategoryName, int.Parse(x.CategoryID), int.Parse(x.CategoryParentID[0]),
                                    x.CategoryLevel)));

                        Categories = new ObservableCollection<CategoryModel>(list);
                        RaisePropertyChanged("Categories");
                    }
                }
            }
            else Directory.CreateDirectory("Categories");

            
            Visibility = false;
            Aspects = new ObservableCollection<AspectsModel>();
            Conditions = new ObservableCollection<ConditionHistogram>();
            PriceFromTo = new List<double>(2);


            showOnly = new List<AspectItem>
            {
                new AspectItem("Returns accepted", 0, TextWrapping.NoWrap, DataTemplateSelectorEnum.CheckBox),
                new AspectItem("Completed listings", 0, TextWrapping.NoWrap, DataTemplateSelectorEnum.CheckBox),
                new AspectItem("Sold listings", 0, TextWrapping.NoWrap, DataTemplateSelectorEnum.CheckBox),
                new AspectItem("PayPal accepted", 0, TextWrapping.NoWrap, DataTemplateSelectorEnum.CheckBox),
                new AspectItem("Listed as lots", 0, TextWrapping.NoWrap, DataTemplateSelectorEnum.CheckBox),
                new AspectItem("Accepts best offer", 0, TextWrapping.NoWrap, DataTemplateSelectorEnum.CheckBox),
                new AspectItem("Sale items", 0, TextWrapping.NoWrap, DataTemplateSelectorEnum.CheckBox),
                new AspectItem("eBay Giving Works", 0, TextWrapping.NoWrap, DataTemplateSelectorEnum.CheckBox)
            };


            /*
             threadsCount = new List<Task>();
             Categories = new ObservableCollection<CategoryModel>(list);
             BuildCategoryHierarchical(Categories.ToList());
             Task.WaitAll(threadsCount.ToArray());*/
        }

        public string TemplateName { get; set; }
        public bool UpdateVisibility { get; set; }
        public ObservableCollection<AspectsModel> Aspects { get; set; }
        public ObservableCollection<ConditionHistogram> Conditions { get; set; }
        public ObservableCollection<CategoryModel> Categories { get; set; }
        public RelayCommand UpdateCommand { get; set; }
        public int SelectedCategoryId { get; set; }
        public bool SelectMode { get; set; }
        public List<double> PriceFromTo { get; set; }
        public bool VisibilityDelete { get; set; }
        public RelayCommand<Window> DeleteCommand { get; set; }

        public bool Visibility
        {
            get { return visibility; }
            set
            {
                visibility = value;
                RaisePropertyChanged("Visibility");
            }
        }

        public bool DeleteTemplate { get; set; }
        public RelayCommand<Window> SaveCommand { get; set; }
        public string SelectedCategoryName { get; set; }

        public bool SelectCategory
        {
            get { return false; }
            set
            {
                if (value)
                {
                    if (CategoryModel.SelectedCategory != null)
                    {
                        Visibility = true;
                        SetCategoryAspects(CategoryModel.SelectedCategory.CategoryId);

                        SelectedCategoryName = CategoryModel.SelectedCategory.CategoryName;
                    }
                }
            }
        }

        private void UpdateCommandExecute()
        {
            if (EbayApiData.Instance().CategoryTypes == null || EbayApiData.Instance().CategoryTypes.Count == 0)
            {
                UpdateVisibility = true;
                RaisePropertyChanged("UpdateVisibility");

                Task.Run(() => { EbayApiData.Instance().SetEbayApiCategories(); })
                    .ContinueWith(i =>
                    {
                        categorytype = EbayApiData.Instance().CategoryTypes;
                        var directory = "Categories";
                        if (!categorytype.IsNullOrEmpty() && Directory.Exists(@directory))
                        {
                            var list = new List<CategoryModel>();
                            list.AddRange(categorytype.Where(x => x.CategoryLevel == 1)
                                .Select(x => new CategoryModel(x.CategoryName, int.Parse(x.CategoryID), int.Parse(x.CategoryParentID[0]),
                                        x.CategoryLevel)));

                            Categories = new ObservableCollection<CategoryModel>(list);
                            RaisePropertyChanged("Categories");

                            var file = "CategoriesTempl";
                            EbaySearcherHelper.SaveAsBinaryFormat(categorytype, file, directory);
                        }

                        UpdateVisibility = false;
                        RaisePropertyChanged("UpdateVisibility");
                    });
            }
        }

        private void DeleteCommandExecute(Window window)
        {
            if (Directory.Exists(@"SaveTemplates"))
            {
                foreach (var file in
                    new DirectoryInfo(@"SaveTemplates").EnumerateFiles(TemplateName + ".dat",
                        SearchOption.AllDirectories))
                {
                    file.Delete();
                }
            }
            else
            {
                Directory.CreateDirectory("SaveTemplates");
            }
            DeleteTemplate = true;
            window.DialogResult = true;
            window.Close();
        }

        private void SaveCommandExecute(Window window)
        {
            window.DialogResult = true;
            window.Close();
        }

        public void BuildCategoryHierarchical(List<CategoryModel> categoryModel)
        {
            Task task;

            foreach (var item in categoryModel)
            {
                var category = item;
                threadsCount.Add(task = Task.Run(() =>
                {
                    category.CategoriesNextLevel.AddRange(
                        categorytype.Where(
                            x =>
                                x.CategoryLevel == category.CategoryLevel + 1 &&
                                int.Parse(x.CategoryParentID[0]) == category.CategoryId).Select(x =>
                                    new CategoryModel(x.CategoryName, int.Parse(x.CategoryID),
                                        int.Parse(x.CategoryParentID[0]),
                                        x.CategoryLevel)));
                    BuildCategoryHierarchical(category.CategoriesNextLevel);
                }));
            }
            //task.Wait();
        }

        public void SetCategoryAspects(int categoryId)
        {
            Task<List<AspectsModel>>.Factory.StartNew(() =>
            {
                var aspectsList = new List<AspectsModel>();
                var service = new CustomFindingService("getHistograms");
                service.Url = "http://svcs.ebay.com/services/search/FindingService/v1";
                //service.v
                var abcd = new GetHistogramsRequest();
                abcd.categoryId = categoryId.ToString();
                var response = service.getHistograms(abcd);

                if (response.conditionHistogramContainer != null
                    && response.conditionHistogramContainer.conditionHistogram != null)
                {
                    var wrap = (response.conditionHistogramContainer.conditionHistogram.Length > 20)
                        ? TextWrapping.Wrap
                        : TextWrapping.NoWrap;
                    var aspectsItemList = response.conditionHistogramContainer.conditionHistogram
                        .Where(x => !string.IsNullOrEmpty(x.condition.conditionDisplayName))
                        .Select(
                            x =>
                                new AspectItem(x.condition.conditionDisplayName, x.count, wrap,
                                    DataTemplateSelectorEnum.CheckBox)).ToList();

                    aspectsList.Add(new AspectsModel("Condition", aspectsItemList));
                }

                if (response.aspectHistogramContainer != null
                    && response.aspectHistogramContainer.aspect != null)
                {
                    foreach (var aspect in response.aspectHistogramContainer.aspect
                        .Where(x => !string.IsNullOrEmpty(x.name)))
                    {
                        var wrap = (aspect.valueHistogram.Length > 20)
                            ? TextWrapping.Wrap
                            : TextWrapping.NoWrap;

                        aspectsList.Add(new AspectsModel(aspect.name, aspect.valueHistogram
                            .Select(x => new AspectItem(x.valueName, x.count, wrap, DataTemplateSelectorEnum.CheckBox))
                            .ToList()));
                    }
                }

                var priceList = new List<AspectItem>();
                priceList.Add(new AspectItem(string.Empty, 0, TextWrapping.NoWrap, DataTemplateSelectorEnum.Price));
                aspectsList.Add(new AspectsModel("Price", priceList));

                return aspectsList;
            }).ContinueWith(result =>
            {
                if (CategoryModel.SelectedCategory.CategoryId == categoryId)
                {
                    result.Result.Add(new AspectsModel("Show only", showOnly));
                    Aspects = new ObservableCollection<AspectsModel>(result.Result.OrderBy(x => x.Title));
                    SelectedCategoryId = categoryId;
                    RaisePropertyChanged("Aspects");
                    RaisePropertyChanged("SelectedCategoryName");
                    Visibility = false;
                }
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}