﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using ebaySearcher.com.ebay.developer;
using System.Threading.Tasks;

namespace ebaySearcher.CategoriesAndAspects
{
    class EbayApiData
    {
        static EbayApiData uniqueInstance;

        public int CategorySiteID { get; set; }
        public List<CategoryType> CategoryTypes { get; set; }

        protected EbayApiData()
        {
        }

        public static EbayApiData Instance()
        {
            if (uniqueInstance == null)
                uniqueInstance = new EbayApiData();

            return uniqueInstance;
        }

        public void SetEbayApiCategories()
        {
            var list = new List<CategoryType>();
            Task.Run(() =>
            {
                EbayTradingApiConnect connect = new EbayTradingApiConnect("GetCategories");
                eBayAPIInterfaceService service = connect.GetEbayAccess();
                GetCategoriesRequestType gercatreq = new GetCategoriesRequestType();
                gercatreq.CategorySiteID = CategorySiteID.ToString();
                gercatreq.Version = ConfigurationManager.AppSettings["Version"];
                gercatreq.DetailLevel = new DetailLevelCodeType[] { DetailLevelCodeType.ReturnAll };
                GetCategoriesResponseType getcatpes = service.GetCategories(gercatreq);
                list = getcatpes.CategoryArray.ToList();
            }).ContinueWith(i => 
            { CategoryTypes = list; }).Wait();

        }
    }

    class Singleton
    {
        static Singleton uniqueInstance;
        string singletonData = string.Empty;

        protected Singleton()
        {
        }

        public static Singleton Instance()
        {
            if (uniqueInstance == null)
                uniqueInstance = new Singleton();

            return uniqueInstance;
        }

        public void SingletonOperation()
        {
            singletonData = "SingletonData";
        }

        public string GetSingletonData()
        {
            return singletonData;
        }
    }
}
