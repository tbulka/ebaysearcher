﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ebaySearcher.CategoriesAndAspects;

namespace ebaySearcher.CategoriesAndAspects
{
    /// <summary>
    /// Interaction logic for Aspects.xaml
    /// </summary>
    public partial class Aspects : Window
    {
        public Aspects(AspectsModelView vw)
        {
            InitializeComponent();
            DataContext = vw;
        }
    }
}
