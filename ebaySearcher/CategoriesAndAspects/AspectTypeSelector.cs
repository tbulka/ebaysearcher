﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ebaySearcher.CategoriesAndAspects
{
    public class AspectTypeSelector : DataTemplateSelector
    {

        public DataTemplate PriceTypeDataTemplate { get; set; }
        public DataTemplate RadioButtonTypeDataTemplate { get; set; }
        public DataTemplate CheckBoxTypeDataTemplate { get; set; }


        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            AspectItem aspectItem = (AspectItem)item;

            switch (aspectItem.DataTemplateSelector)
            {
                case DataTemplateSelectorEnum.RadioButton:
                    return RadioButtonTypeDataTemplate;
                case DataTemplateSelectorEnum.CheckBox:
                    return CheckBoxTypeDataTemplate;
                case DataTemplateSelectorEnum.Price:
                    return PriceTypeDataTemplate;
                default:
                    return null;
            }
        }
    }
}
