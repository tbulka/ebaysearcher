﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ebaySearcher.CategoriesAndAspects;
using ebaySearcher.EditAspectTemplate;

namespace ebaySearcher.ChoosenAspectValues
{
    /// <summary>
    /// Interaction logic for SelectedAspectValues.xaml
    /// </summary>
    public partial class SelectedAspectValues : UserControl
    {
        public SelectedAspectValues()
        {
            InitializeComponent();
        }

        [CommonDependencyProperty]
        public static readonly DependencyProperty AspectValuesSourceProperty =
            DependencyProperty.Register("AspectValuesSource", typeof(IEnumerable), typeof(SelectedAspectValues),
            new FrameworkPropertyMetadata(string.Empty));

        public static readonly DependencyProperty TitleProperty =
           DependencyProperty.Register("Title", typeof(string), typeof(SelectedAspectValues),
           new FrameworkPropertyMetadata(string.Empty));

        public static readonly DependencyProperty AspectAllValuesSourceProperty =
           DependencyProperty.Register("AspectAllValuesSource", typeof(IEnumerable), typeof(SelectedAspectValues),
           new FrameworkPropertyMetadata(string.Empty));

        [Bindable(true), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public IEnumerable AspectValuesSource
        {
            get
            { return (IEnumerable)GetValue(AspectValuesSourceProperty); }
            set
            {
                if (value == null)
                {
                    ClearValue(AspectValuesSourceProperty);
                }
                else
                {
                    SetValue(AspectValuesSourceProperty, value);
                }
            }
        }

        public IEnumerable AspectAllValuesSource
        {
            get
            { return (IEnumerable)GetValue(AspectAllValuesSourceProperty); }
            set
            {
                if (value == null)
                {
                    ClearValue(AspectAllValuesSourceProperty);
                }
                else
                {
                    SetValue(AspectAllValuesSourceProperty, value);
                }
            }
        }


        public string Title
        {
            get
            { return (string)GetValue(TitleProperty); }
            set
            {
                SetValue(TitleProperty, value);
            }
        }

        private void EditButton_OnClick(object sender, RoutedEventArgs e)
        {
            EditAspectModelView editAspectMv = new EditAspectModelView();
            EditAspect editAspect = new EditAspect(editAspectMv);
            editAspectMv.AspectValues = new ObservableCollection<AspectItem>(AspectAllValuesSource.OfType<AspectItem>().ToList());
            //editAspectMv.AddFuncion = AddFuncion;
            editAspect.ShowDialog();
            if (editAspect.DialogResult == true)
            {
                //AspectAllValuesSource = editAspectMv.AspectValues.Where(x=>x.IsChecked);
                var list = editAspectMv.AspectValues.Where(x => x.IsChecked);
                foreach (var item in list.Where(x=>x.DataTemplateSelector.Equals(DataTemplateSelectorEnum.Price)))
                    if (item.DataTemplateSelector.Equals(DataTemplateSelectorEnum.Price))
                        item.Title = item.PriceCondition.ToString();
                ItemsControl1.ItemsSource = list.ToList();
                //AspectValues = new ObservableCollection<AspectItem>(editAspectMv.AspectValues);
                //RaisePropertyChanged("TestItemsControl");
                //RaisePropertyChanged("CheckedAspectValues");
            }
        }
    }

    [Conditional("COMMONDPS")]
    internal sealed class CommonDependencyPropertyAttribute : Attribute
    {
    }
}

