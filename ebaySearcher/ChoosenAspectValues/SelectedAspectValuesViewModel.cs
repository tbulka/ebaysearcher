﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using ebaySearcher.CategoriesAndAspects;
using ebaySearcher.EditAspectTemplate;
using Microsoft.TeamFoundation.MVVM;
using MVVMbase;

namespace ebaySearcher.ChoosenAspectValues
{
   public class SelectedAspectValuesViewModel : NotificationObject
    {
       public RelayCommand<ItemsControl> EditAspectCommand { get; set; }
       public ObservableCollection<AspectItem> AspectValues { get; set; }
       public IEnumerable<AspectItem> TestItemsControl { get; set; }
        
       public SelectedAspectValuesViewModel()
       {
           EditAspectCommand = new RelayCommand<ItemsControl>(EditAspectCommandExecute);
       }

       private void EditAspectCommandExecute(ItemsControl collection)
       {
           //TestItemsControl = collection.ItemsSource.OfType<AspectItem>();
           //EditAspectModelView editAspectMv = new EditAspectModelView();
           //EditAspect editAspect = new EditAspect(editAspectMv);
           //editAspectMv.AspectValues = TestItemsControl.ToList();
           ////editAspectMv.AddFuncion = AddFuncion;
           //editAspect.ShowDialog();
           //if (editAspect.DialogResult == true)
           //{
           //    //AspectValues = new ObservableCollection<AspectItem>(editAspectMv.AspectValues);
           //    RaisePropertyChanged("TestItemsControl");
           //    //RaisePropertyChanged("CheckedAspectValues");
           //}
       }



    }
}
