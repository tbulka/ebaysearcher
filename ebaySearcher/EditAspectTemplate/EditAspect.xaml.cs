﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ebaySearcher.EditAspectTemplate
{
    /// <summary>
    /// Interaction logic for EditAspect.xaml
    /// </summary>
    public partial class EditAspect : Window
    {
        public EditAspect(EditAspectModelView vw)
        {
            InitializeComponent();
            DataContext = vw;
        }
    }
}
