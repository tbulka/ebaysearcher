﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ebaySearcher.CategoriesAndAspects;
using MVVMbase;

namespace ebaySearcher.EditAspectTemplate
{
    public class EditAspectModelView : NotificationObject
    {
        private ObservableCollection<AspectItem> aspectValues;
        public ObservableCollection<AspectItem> AspectValues
        {
            get { return aspectValues; }
            set
            {
                if (value.Count == 1)
                    AddFuncion = false;//////// Змінити

                aspectValues = value;
            }
        }
        public string NewAspectValue { get; set; }
        public bool AddFuncion { get; set; }
        public RelayCommand<Window> OkCommand { get; set; }
        public ICommand AddCommand { get; set; }
        public List<AspectItem> CheckedAspectValues
        {
            get
            {
                var checkedValuesList = AspectValues.Where(x => x.IsChecked).ToList();
                return checkedValuesList;
            }
        }
        public EditAspectModelView()
        {
            AddFuncion = true;
            AddCommand = new RelayCommand(AddCommandExecute);
            //CancelCommand = new RelayCommand(CancelCommandExecute);
            OkCommand = new RelayCommand<Window>(OkCommandExecute);
            AspectValues = new ObservableCollection<AspectItem>();
        }

        private void AddCommandExecute()
        {
            AspectValues.Add(new AspectItem(NewAspectValue, AspectValues.FirstOrDefault().DataTemplateSelector));
            RaisePropertyChanged("AspectValues");
        }

        private void OkCommandExecute(Window window)
        {
            window.DialogResult = true;
            window.Close();
        }
    }
}
